package ir.enarije.alidavoudi.elahecalculator;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import ir.enarije.alidavoudi.elahecalculator.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {


    private ActivityMainBinding binding; //view binding
    private boolean isOpPressed = false;
    private double firstNumber = 0;
    private int secondNumberIndex = 0;
    private char currentOp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //view binding
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        final View.OnClickListener calculatorListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int id = v.getId();
                final String screenContent = binding.display.getText().toString();
                switch (id) {
                    case R.id.num0:
                        binding.display.append("0");
                        break;
                    case R.id.num1:
                        binding.display.append("1");
                        break;
                    case R.id.num2:
                        binding.display.append("2");
                        break;
                    case R.id.num3:
                        binding.display.append("3");
                        break;
                    case R.id.num4:
                        binding.display.append("4");
                        break;
                    case R.id.num5:
                        binding.display.append("5");
                        break;
                    case R.id.num6:
                        binding.display.append("6");
                        break;
                    case R.id.num7:
                        binding.display.append("7");
                        break;
                    case R.id.num8:
                        binding.display.append("8");
                        break;
                    case R.id.num9:
                        binding.display.append("9");
                        break;
                    case R.id.dot:
                        binding.display.append(".");
                        break;
                    case R.id.equal:
                        if (isOpPressed) {
                            if (currentOp == '+') {
                                String secondNumberString = screenContent.
                                        substring(secondNumberIndex, screenContent.length());
                                double secondNumber = Double.parseDouble(secondNumberString);
                                firstNumber += secondNumber;
                                binding.display.setText(String.valueOf(firstNumber));
                            }
                            if (currentOp == '-') {
                                String secondNumberString = screenContent.
                                        substring(secondNumberIndex, screenContent.length());
                                double secondNumber = Double.parseDouble(secondNumberString);
                                firstNumber -= secondNumber;
                                binding.display.setText(String.valueOf(firstNumber));
                            }
                            if (currentOp == '*') {
                                String secondNumberString = screenContent.
                                        substring(secondNumberIndex, screenContent.length());
                                double secondNumber = Double.parseDouble(secondNumberString);
                                firstNumber *= secondNumber;
                                binding.display.setText(String.valueOf(firstNumber));
                            }
                            if (currentOp == '/') {
                                String secondNumberString = screenContent.
                                        substring(secondNumberIndex, screenContent.length());
                                double secondNumber = Double.parseDouble(secondNumberString);
                                firstNumber /= secondNumber;
                                binding.display.setText(String.valueOf(firstNumber));
                            }
                            if (currentOp == '%') {
                                String secondNumberString = screenContent.
                                        substring(secondNumberIndex, screenContent.length());
                                double secondNumber = Double.parseDouble(secondNumberString);
                                firstNumber %= secondNumber;
                                binding.display.setText(String.valueOf(firstNumber));
                            }
                        }
                        break;
                    case R.id.addition:
                        secondNumberIndex = screenContent.length() + 1;
                        firstNumber = Double.parseDouble(screenContent);
                        binding.display.append("+");
                        isOpPressed = true;
                        currentOp = '+';
                        break;
                    case R.id.subtraction:
                        secondNumberIndex = screenContent.length() + 1;
                        firstNumber = Double.parseDouble(screenContent);
                        binding.display.append("-");
                        isOpPressed = true;
                        currentOp = '-';
                        break;
                    case R.id.multiplication:
                        secondNumberIndex = screenContent.length() + 1;
                        firstNumber = Double.parseDouble(screenContent);
                        binding.display.append("*");
                        isOpPressed = true;
                        currentOp = '*';
                        break;
                    case R.id.division:
                        secondNumberIndex = screenContent.length() + 1;
                        firstNumber = Double.parseDouble(screenContent);
                        binding.display.append("/");
                        isOpPressed = true;
                        currentOp = '/';
                        break;
                    case R.id.mode:
                        secondNumberIndex = screenContent.length() + 1;
                        firstNumber = Double.parseDouble(screenContent);
                        binding.display.append("%");
                        isOpPressed = true;
                        currentOp = '%';
                        break;

                }
            }

        };

        binding.num0.setOnClickListener(calculatorListener);
        binding.num1.setOnClickListener(calculatorListener);
        binding.num2.setOnClickListener(calculatorListener);
        binding.num3.setOnClickListener(calculatorListener);
        binding.num4.setOnClickListener(calculatorListener);
        binding.num5.setOnClickListener(calculatorListener);
        binding.num6.setOnClickListener(calculatorListener);
        binding.num7.setOnClickListener(calculatorListener);
        binding.num8.setOnClickListener(calculatorListener);
        binding.num9.setOnClickListener(calculatorListener);
        binding.dot.setOnClickListener(calculatorListener);
        binding.equal.setOnClickListener(calculatorListener);
        binding.addition.setOnClickListener(calculatorListener);
        binding.subtraction.setOnClickListener(calculatorListener);
        binding.multiplication.setOnClickListener(calculatorListener);
        binding.division.setOnClickListener(calculatorListener);
        binding.mode.setOnClickListener(calculatorListener);

        binding.delete.setOnClickListener(v -> {

            String displayedElements = binding.display.getText().toString();
            int lenght = displayedElements.length();
            if (lenght > 0) {
                displayedElements = displayedElements.substring(0, lenght - 1);
                binding.display.setText(displayedElements);
            }
        });
        binding.clear.setOnClickListener(v -> {

            binding.display.setText("");

        });

       binding.MenuBurger.setOnClickListener(v -> {
           binding.cellTitleView.setVisibility(View.GONE);
           binding.cellContentView.setVisibility(View.VISIBLE);
       });
       binding.remove.setOnClickListener(v -> {
           binding.cellContentView.setVisibility(View.GONE);
           binding.cellTitleView.setVisibility(View.VISIBLE);
       });

    }
}
